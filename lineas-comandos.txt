Lineas de comandos para creacion componentes e instalaciones en angular:

# Recomendacion: al trabajar en un proyecto en angular debemos trabajar de forma modularizada; es decir que primero
debemos de crear los módulos y luego los componentes y servcios como tambien crear una carpeta de services y pages 
(para los componentes)

* linea de comando para crear servicios:
ng g s auth/services/auth --skip-tests

* linea de comando para crear guards:
ng g guard auth/guards/auth --skip-tests

* linea de comando para crear componente:
ng g c heroes/confirmar --skip-tests -is

* linea de comando para crear pipe:
ng g p heroes/pipes/imagen --skip-tests

* linea de comando para instalar Angular Flex en mi proyecto:
npm i -s @angular/flex-layout @angular/cdk

* linea de comando para instalar Angular Material en mi proyecto:
ng add @angular/material

* linea de comando para crear un archivo de rutas manualmente (ruta principal)
ng g m heroes/appRouting --flat

* linea de comando para crear un módulo y archivo de rutas
ng g m template --routing

* linea de comando para levantar un fake api rest en json-server
json-server --watch db.json

* linea de comando para publicar nuestra app de angular
ng build --configuration production
