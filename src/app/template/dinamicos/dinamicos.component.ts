import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';


interface Persona {
  nombre: string;
  favoritos: Favorito[];
}

interface Favorito{
  id: number;
  nombre: string;
}

@Component({
  selector: 'app-dinamicos',
  templateUrl: './dinamicos.component.html',
  styles: [
  ]
})
export class DinamicosComponent implements OnInit {

  @ViewChild('miForm') form!: NgForm;
  nuevoJuego!: string;

  //creacion de un objeto
  persona : Persona = {
    nombre: 'Marcos',
    favoritos: [
      {
        id: 1,
        nombre: 'super mario'
      },
      {
        id: 2,
        nombre: 'metal gear'
      }
    ]
  }

  constructor() { }

  ngOnInit(): void {
  }

  nombreValido(): boolean{
    return this.form?.controls['nombre']?.invalid 
        && this.form?.controls['nombre']?.touched
        && this.form?.controls['nombre']?.errors?.['required']
  }

  guardar(){
    console.log('guardar')
  }

  eliminar(indice: number){
    console.log('indice', indice);
    this.persona.favoritos.splice(indice,1)
  }

  agregar(){
    this.form?.controls['favorito']?.value;
    const newFav : Favorito  = {
      id: this.persona.favoritos.length+1,
      nombre: this.nuevoJuego
      // //other way : si tenemos en el html de esta manera el ngModel sin la caja de banana two data binding
      // nombre: this.form?.controls['favorito']?.value
    }

    //utilizamos el operador spread para realizar una copia de objeto original y agregarlo al arreglo de favoritos
    this.persona.favoritos.push({...newFav});
    // console.log('this.form?.controls[favorito]?.value', this.form?.controls['favorito']?.value);
    this.nuevoJuego ='';
  }

}
