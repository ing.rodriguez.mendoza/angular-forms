import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent implements OnInit {

  //referencia local que queremos buscar
  @ViewChild('miFormulario') form!: NgForm;

  constructor() { }

  ngOnInit(): void {
  }

  //inicializando nuestro formulario con aproximacion de template
  initForm = {
    producto : '',
    precio: 0,
    existencia: 0
  }


  guardar(){
    console.log(this.form);
    //restablece los valores del formulario a su estado inicial
    this.form.resetForm({
      precio: 0,
      existencia: 0
    });
  }

  nombreValido(): boolean
  {
    // console.log('ingreso nombreValido');
    return this.form?.controls['producto']?.invalid 
        && this.form?.controls['producto']?.touched;    
  }

  precioValido(): boolean
  {
    // console.log('ingreso precioValido');
    // && this.form?.controls['precio']?.value < 0
    return this.form?.controls['precio']?.invalid
        && this.form?.controls['precio']?.touched
        && this.form?.controls['precio']?.errors?.['min'];
  }

  precioRequerido() : boolean
  {
    return this.form?.controls['precio']?.invalid 
        && this.form?.controls['precio']?.touched
        && this.form?.controls['precio']?.errors?.['required'];    
  }

  existenciaValido(): boolean
  {
    // console.log('ingreso existenciaValido');
    if (this.form?.controls['existencia']?.invalid 
     && this.form?.controls['existencia']?.touched) 
    {
      return true;
    }
    return false;
  }

  // //other way
  // guardar(miFormulario: NgForm){
  //   console.log('miFormulario', miFormulario);
  // }

}
