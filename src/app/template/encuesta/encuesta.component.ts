import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-encuesta',
  templateUrl: './encuesta.component.html',
  styles: [
  ]
})
export class EncuestaComponent {
  
  texto: string= 'pregunta';
  tag: string= 'radio';
  
  preguntas = [    
    { id:1 , valor: '' },
    { id:2 , valor: '' },
    { id:3 , valor: '' },
    { id:4 , valor: '' },
    { id:5 , valor: '' },
    { id:6 , valor: '' },
    { id:7 , valor: '' },
    { id:8 , valor: '' },
    { id:9 , valor: '' },
    { id:10 , valor: '' },
  ]
  
  valOpcion01: number = 1;
  valOpcion02: number = 4;
  valOpcion03: number = 6;

  guardar(){
    console.log('this.preguntas', (this.preguntas));
    const initialValue = 0;
    const sum = this.preguntas.reduce(
      (resp, act) =>  Number(act.valor) + resp , initialValue
    );
    console.log('sum', sum)
  }  
}

// console.log('this.preguntas', JSON.stringify(this.preguntas));

// const sum = this.preguntas.pregunta01 + this.preguntas.pregunta02 + this.preguntas.pregunta03;

  // preguntas = {
  //   pregunta01: 0,
  //   pregunta02: 0,
  //   pregunta03: 0,
  //   pregunta04: 0,
  //   pregunta05: 0,
  //   pregunta06: 0,
  //   pregunta07: 0,
  //   pregunta08: 0,
  //   pregunta09: 0,
  //   pregunta10: 0,
  // }
    // console.log('sum total: ', sum);