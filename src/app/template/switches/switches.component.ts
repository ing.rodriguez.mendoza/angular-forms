import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-switches',
  templateUrl: './switches.component.html',
  styles: [
  ]
})
export class SwitchesComponent {

  @ViewChild('miForm') form!: NgForm;

  persona = {
    genero : '',
    notificacion: true,
  }

  terminosYcondiciones: boolean = false;

  constructor() { 
      
  }

  validate(){
     return this.form?.controls['termino']?.errors
  }
}
