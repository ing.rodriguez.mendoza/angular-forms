import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidemenuComponent } from './sidemenu/sidemenu.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [
    SidemenuComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [//ya que los componentes se van a utilizar fuera del SharedModule tenemos que exportarlo
    SidemenuComponent
  ]
})
export class SharedModule { }
