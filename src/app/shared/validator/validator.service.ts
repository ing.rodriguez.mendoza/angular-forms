import { Injectable } from '@angular/core';
import { AbstractControl, FormControl, ValidationErrors } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {
  
  nombreApellidoPattern : string = '([a-zA-Z]+) ([a-zA-z]+)';
  emailPattern: string = "^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";

  constructor() { }
  
  noPuedeSerStrider = ( control: FormControl ) : ValidationErrors | null => {
    const valor: string = control.value?.trim().toLowerCase();
    if (valor === 'strider') {
      return { noStrider: true }
    }
    return null;
  }

  // console.log('formgroup',formgroup):
  camposIguales(campo1: string, campo2: string){
    return (formgroup: AbstractControl) : ValidationErrors | null => {
      const cont01 = formgroup.get(campo1)?.value;
      const cont02 = formgroup.get(campo2)?.value;
      if (cont01 !== cont02) {
        formgroup.get(campo2)?.setErrors({ noIguales : true });
        return { noIguales : true }
      }
      //cuando no hay errores
      formgroup.get(campo2)?.setErrors( null );
      return null
    }
  }

}
