import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AbstractControl, AsyncValidator, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { delay, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmailValidatorService implements AsyncValidator {

  private url: string = 'http://localhost:3000';

  constructor(private http:HttpClient) { }

  validate(control: AbstractControl): Observable<ValidationErrors | null> {
    const email = control.value;
    console.log(email);
    return this.http.get<any[]>(`${this.url}/usuarios?q=${email}`)
                        .pipe(
                              delay(3000),
                              map( resp => { return resp.length === 0 ? null : { eamilExiste: true } })
                             );
  }

}
