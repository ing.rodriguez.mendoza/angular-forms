import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { from, fromEvent, Observer, of } from 'rxjs';
import { distinct, distinctUntilChanged, filter, map, pluck, skip, take, takeWhile, tap } from 'rxjs/operators';


@Component({
  selector: 'app-ejercicio',
  templateUrl: './ejercicio.component.html',
  styleUrls: ['./ejercicio.component.css']
})

export class EjercicioComponent {

  @ViewChild('txtTermino')  txtTermino01!: ElementRef<HTMLInputElement>;
  @ViewChild('txtTermino2') txtTermino02!: ElementRef<HTMLInputElement>;
  
  constructor(private  fb: FormBuilder) { }
  
  // //inicializamos los campos de mi formulario
  miForm: FormGroup = this.fb.group(
    {
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      favoritos: this.fb.array([ // creamos un arreglo de elementos
          ['Play Station'],
          ['Nintento WIII']
        ]
      , Validators.required)//.disable()
      // favoritos: [] //creacion de un nuevo campo
    }
  )

  nuevoFavorito = this.fb.control( '', [Validators.required, Validators.maxLength(4), Validators.max(4), Validators.pattern('[0-9]{4}')] );
  
  get favoritosArr() {
    return this.miForm.get('favoritos') as FormArray;  }

  campoNoValido(campo: string) {
    return this.miForm.controls[campo].errors && this.miForm.controls[campo].touched;
  }

  inputDisable: boolean = false;

  agregar(x: any){
    //console.log('e', x);

    //#region solo numeros
    var charCode = (x.which) ? x.which : x.keyCode;
    // Only Numbers 0-9
    if ((charCode < 48 || charCode > 57)) {
      x.preventDefault();
      return false;
    } else {
      ////
        
    //keyup: si se suelta la tecla del teclado valga la redundancia
    // const input$ = fromEvent<KeyboardEvent>( this.txtTermino01.nativeElement, 'keyup');
    //keydown: si se presiona la tecla del teclado valga la redundancia
    const input$ = fromEvent<KeyboardEvent>( this.txtTermino01.nativeElement, 'keydown');
    //const input02$ = fromEvent<KeyboardEvent>( this.txtTermino01.nativeElement, 'keydown');
    
    const observer: Observer<any> = {
      next : value => console.log('siguiente [next]:', value ),
      error: error => console.warn('error [obs]:', error ),
      complete: () => {
        console.info('completado [obs]');
        //para deshabilitar el boton
        // this.txtTermino01.nativeElement.disabled = true;
        //para establecer el foco en el input con referencia local #txtTermino2
        this.txtTermino02.nativeElement.focus();
        console.dir(this.txtTermino02.nativeElement);
      }
    };

    input$
    .pipe(
        //distinct(),
        //tap( (tap) => console.log('tap before skip', tap) ),              
        //skip(3),//a la cuarta emision recien muestra (salida) resultado (omite las emisiones iniciales tres en total)
        tap( (tap) => console.log('tap after skip', tap) ),      
        map<any,string>(
          ({target}) => target.value 
        ),
        tap( resp => console.log('tap', resp) ),
        takeWhile<any>( resp=> resp.length < 4 , true ),//tomamos el valor si el total de caracteres es igual a 4 que necesitamos
        tap( resp => console.log('tap length', resp.length) ),
      )
      .subscribe( observer );
      ////
      return true;
    }
    //#endregion
    
    // //console.log('this.miForm', this.miForm);

    // if (this.nuevoFavorito.invalid) {
    //   return;
    // }

    // ////other way
    // //this.favoritosArr.push( new FormControl( this.nuevoFavorito.value, Validators.required ) );
    
    // this.favoritosArr.push( this.fb.control( this.nuevoFavorito.value, Validators.required ))

    // console.log('this.favoritosArr.value',this.favoritosArr.value)
    
    // //limpiar el control
    // this.nuevoFavorito.reset();

    /////

  }

  eliminar(indice: number){
    console.log('indice', indice);
    this.favoritosArr.removeAt(indice);
  }

  guardar(){
    if (this.miForm.invalid) {
      this.miForm.markAllAsTouched();
      return;
    }
    console.log('guardar', this.miForm.value);
  }
}


