import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-basicos',
  templateUrl: './basicos.component.html',
  styles: [
  ]
})
export class BasicosComponent implements OnInit {

  // // other way : uso para formulario no complejos de pocos campos
  // miForm : FormGroup = new FormGroup(
  //   {
  //     'nombre' : new FormControl('PLAY STATION'),
  //     'precio' : new FormControl(0),
  //     'existencia' : new FormControl(0),
  //   }
  // )

  ngOnInit(): void {
      //inizializamos el formulario con valores      
      this.miForm.reset(
        {
          nombre      : '',
          precio      : 0,
          // existencia  : 0
        }
      )

      ////other way: no es recomendable utilizar setValue para inicializar un formulario 
      ////           lo recomendable es utilizar reset ya que no nos fuerza a inicializar todos
      ////           los valores
      // this.miForm.setValue(
      //   {
      //     nombre      : '',
      //     precio      : 0,
      //     existencia  : 0
      //   }
      // )

  }
  
  constructor( private fb: FormBuilder ) { }
  
  miForm : FormGroup = this.fb.group(
    {
      nombre      : ['', //valor asignado
                    [Validators.required, Validators.minLength(3)]], //validaciones sincronas
      precio      : [,
                    [Validators.required, Validators.min(0)]],
      existencia  : [,
                    [Validators.required, Validators.min(0)]],
    }
  )

  campoInValido(campo: string){
    return this.miForm.controls[campo].errors && this.miForm.controls[campo].touched;
    ////other way
    // return this.miForm.controls['existencia'].errors && this.miForm.controls['existencia'].touched;
    // return this.miForm.controls['existencia'].errors; //&& this.miForm.controls['existencia'].touched;
  }

  guardar(){
    if (this.miForm.invalid) {//si el formulario no es válido 
      this.miForm.markAllAsTouched();//método que permite marcar que todos los campos de mi formulario han sido tocados.
      return;
    }

    console.log('this.miForm', this.miForm.value);
    this.miForm.reset();//método que nos permite inicializar el formulario
  }

  

  
}
