import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, FormControl } from '@angular/forms';
import { filter, from, fromEvent, map, pluck, skip, take, takeWhile, tap } from 'rxjs';

interface Persona {
  nombre: string;
  favoritos: Favorito[];
}

interface Favorito{
  id: number;
  nombre: string;
}

@Component({
  selector: 'app-dinamicos',
  templateUrl: './dinamicos.component.html',
  styles: [
  ]
})
export class DinamicosComponent implements OnInit {

  constructor(private  fb: FormBuilder) { }
  
  // //inicializamos los campos de mi formulario
  miForm: FormGroup = this.fb.group(
    {
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      favoritos: this.fb.array([ // creamos un arreglo de elementos
          ['Play Station'],
          ['Nintento WIII']
        ]
      , Validators.required)//.disable()
      // favoritos: [] //creacion de un nuevo campo
    }
  )

  nuevoFavorito = this.fb.control( '', Validators.required );
  
  get favoritosArr() {
    return this.miForm.get('favoritos') as FormArray;  }
  
  ngOnInit(): void {
    //// other way
    ////inicializamos los campos de mi formulario
    // this.miForm.reset(
    //   {
    //     nombre : ''
    //   }
    // )

    // this.miForm.reset([
    //   {value: 'favoritos', disabled: true}
    // ]);

    // const arr = new FormArray([
    //     new FormControl(),
    //     new FormControl()
    // ]);
    // // arr.reset(['name', 'last name']);
    
    // console.log(arr.value);  // ['name', 'last name']

    // arr.reset([
    //   {value: 'name', disabled: true},
    //   'last'
    // ]);
    
    // console.log(arr.value);  // ['last']
    // console.log(arr.disable());  // ['last']
    // console.log(arr.at(0).status);  // 'DISABLED'

    // this.favoritosArr.controls.map( resp => resp.disable());
  }

  campoNoValido(campo: string) {
    return this.miForm.controls[campo].errors && this.miForm.controls[campo].touched;
  }

  inputDisable: boolean = false;

  agregar(x: any){

    //console.log('e', e);


    

    // //console.log('this.miForm', this.miForm);

    if (this.nuevoFavorito.invalid) {
      return;
    }

    // ////other way
    // //this.favoritosArr.push( new FormControl( this.nuevoFavorito.value, Validators.required ) );
    
    this.favoritosArr.push( this.fb.control( this.nuevoFavorito.value, Validators.required ))

    // console.log('this.favoritosArr.value',this.favoritosArr.value)
    
    // //limpiar el control
    // this.nuevoFavorito.reset();

    /////

  }

  eliminar(indice: number){
    console.log('indice', indice);
    this.favoritosArr.removeAt(indice);
  }

  guardar(){
    if (this.miForm.invalid) {
      this.miForm.markAllAsTouched();
      return;
    }
    console.log('guardar', this.miForm.value);
  }
}


// get favoritosArr() {
//   ////other way de obtener el valor del controls favoritos
//   // return this.miForm.controls['favoritos'] as FormArray;
// }

/**
 
  // //inicializamos los campos de mi formulario
  miForm: FormGroup = this.fb.group(
    {
      nombre: ['', [Validators.required, Validators.minLength(3)]],
      favoritos: this.fb.array([ // creamos un arreglo de elementos
        //  { value: ['Play Station','Nintento WIII'] , disable: true}
          //  ['ddd', false]
          // new FormControl('Nancy', Validators.minLength(2)),          
          //  {value: 'Nintento WIII', disabled: true } ,
          ['Play Station'],
          ['Nintento WIII']
        ]
      , Validators.required)//.disable()
      // favoritos: [] //creacion de un nuevo campo
    }
  )
 
 * 
 */


/**
 

  agregar( e: any ){
    // console.log('e keyup: ', e);
    console.log('this.miForm', this.miForm);

    if (this.nuevoFavorito.invalid) {
      return;
    }


    //agregando un control de forma dinámica a la lista de favoritos
    // this.favoritosArr.push( new FormControl( {value: this.nuevoFavorito.value, disabled: true}, Validators.required ) );
    this.favoritosArr.push( new FormControl( this.nuevoFavorito.value, Validators.required ) );
    
    /////para deshabilitar el boton
    // console.log('favoritosArr.controls', this.favoritosArr.controls.map( resp => resp.disable()));

    this.nuevoFavorito.reset();
  }


 */