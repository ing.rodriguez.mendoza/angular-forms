import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-switches',
  templateUrl: './switches.component.html',
  styles: [
  ]
})
export class SwitchesComponent implements OnInit {

  miFormulario: FormGroup = this.fb.group({
    genero: ['M', Validators.required],
    notificaciones: [ true, Validators.required],
    condiciones: [false, Validators.requiredTrue]
  })

  persona = {
    genero: 'F',
    notificaciones: true
  }

  constructor(private fb:FormBuilder) { }

  ngOnInit(): void {
    // this.miFormulario.setValue( this.persona );
    this.miFormulario.reset({ ...this.persona , condiciones: false });

    // this.miFormulario.valueChanges.subscribe(
    //   ( { condiciones, ...restoCampos } ) => 
    //                                       {
    //                                         this.persona = restoCampos;
    //                                         console.log('condiciones', condiciones);
    //                                         console.log('form', restoCampos);
    //                                       });

    // this.miFormulario.get('condiciones')?.valueChanges.subscribe( condiciones => console.log('camp codiciones', condiciones));

    this.miFormulario.valueChanges.subscribe(
      ( {...form} ) => // con el {...form} obtenemos una copia del objeto original miFormulario
                                          {
                                            delete form.condiciones;//eliminamos el campo condiciones de la copia form 
                                            this.persona = form;
                                            console.log('form', form);
                                          });

  }

  guardar(){
    const formValue = {...this.miFormulario.value};
    //eliminamos el campo condiciones de nuesro formulario
    delete formValue.condiciones;
    console.log(formValue);
  }
}
