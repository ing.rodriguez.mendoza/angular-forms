import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
// import { emailPattern, nombreApellidoPattern, noPuedeSerStrider } from 'src/app/shared/validator/validaciones';
import { ValidatorService } from '../../../shared/validator/validator.service';
import { EmailValidatorService } from '../../../shared/validator/email-validator.service';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  get emailMsgError() : string | null {
    const errors = this.miForm.get('email')?.errors;
    if(errors?.['required']) {
      return 'El email es obligatorio';
    }
    else if(errors?.['pattern']) {
      return 'El email no tiene el formato correcto';
    }
    else if(errors?.['eamilExiste']) {
      return 'El email ingresado ya existe';
    }
    return ''
  }
  

  constructor(
    private fb:FormBuilder,
    private vs:ValidatorService,
    private ev:EmailValidatorService) { }

  miForm : FormGroup = this.fb.group({
    nombre        : [ '', [ Validators.required, Validators.pattern(this.vs.nombreApellidoPattern) ], ],
    email         : [ '', [ Validators.required, Validators.pattern(this.vs.emailPattern) ], [this.ev] ],
    //enviando la referencia de la funcion noPuedeSerStrider no la ejecuion del mismo
    username      : [ '', [ Validators.required, this.vs.noPuedeSerStrider ], ],
    password      : [ '', [ Validators.required, Validators.minLength(6)], ],
    confPassword  : [ '', [ Validators.required, ], ],
  }, {
    //declarando validadores globales
    validators : [ this.vs.camposIguales('password','confPassword' ) ]
  });

  ngOnInit(): void {

    this.miForm.reset({
      nombre        : 'Diana Ysabel',
      email         : 'test1@test.com',
      username      : 'agent22',
      password      : '123456',
      confPassword  : '123456',
    });

  }

  campoNoValido(campo: string) {
    // console.log('this.miForm.get(campo3}
    //)?.invalid && this.miForm.get(campo)?.touched', this.miForm.get(campo)?.invalid && this.miForm.get(campo)?.touched);
    return this.miForm.get(campo)?.invalid 
        && this.miForm.get(campo)?.touched;
  }

  emailRequired() {
    return this.miForm.get('email')?.errors?.['required']
        && this.miForm.get('email')?.touched;
  }

  emailFormat() {
    return this.miForm.get('email')?.errors?.['pattern']
        && this.miForm.get('email')?.touched;
  }

  emailExiste() {
    return this.miForm.get('email')?.errors?.['eamilExiste']
        && this.miForm.get('email')?.touched;
  }

  crear(){
    console.log(this.miForm);
    this.miForm.markAllAsTouched();
  }

}
